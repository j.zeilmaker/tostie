<?php

namespace App\Controller;

use App\Entity\TodoItems;
use App\Entity\TodoList;
use App\Form\TodoListForm;
use App\Repository\TodoListRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/live-component/demos')]
class CollectionController extends AbstractController
{
    #[Route('/form-collection-type/{id}', name: 'app_live_components_demo_form_collection_type', defaults: ['id' => null])]
    public function demoFormCollectionType(Request $request, TodoListRepository $todoListRepository, TodoList $todoList = null): Response
    {
        if (!$todoList) {
            $todoList = new TodoList();
            $todoList->addTodoItem(new TodoItems());
        }
        $form = $this->createForm(TodoListForm::class, $todoList);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $todoListRepository->add($form->getData(), true);
            $this->addFlash('success', 'Excellent! With this to-do list, I won’t miss anything.');

            return $this->redirectToRoute('app_live_components_demo_form_collection_type', [
                'id' => $todoList->getId(),
            ]);
        }

        return $this->renderForm('base.html.twig', [
            'form' => $form,
            'todo' => $todoList,
        ]);
    }

}